# Updates Page

Updates page is opened via popup notification to announce a new version update (when there is something to inform). 

## Design

* [https://eyeogmbh.invisionapp.com/share/CVVGO1J2FQG#/screens](https://eyeogmbh.invisionapp.com/share/CVVGO1J2FQG#/screens)
* Assets: https://zpl.io/bJ161xK and [res/abp/notifications/update-page/](res/abp/notifications/update-page/)
    * hero image: [/res/abp/notifications/updates_panda.gif](/res/abp/notifications/updates_panda.gif)
    * logo image: [/res/abp/logo/ABP-logo-full.svg](/res/abp/logo/ABP-logo-full.svg)

## Notification process

* Information type notification; content is defined per version:
    - Notification title: `Adblock Plus is up-to-date`
    - Notification message: `No action is required—continue browsing the web without annoying ads! <a>See what's new</a>`
* Only shown to desktop users
* Only shown to users with locales of the supported core languages
* Clicking on CTA in popup notification opens the updates.html page

## Header content

* ABP logo - [Documentation link](/spec/abp/prefs.md#documentation-link): %LINK%=adblock_plus in a new tab
* Animation - the animation is different with every version
* Latest ABP version number
* Title and description are different with every version
    * Title: `Adblock Plus is up-to-date!`
    * Description: `We think you'll like the latest updates.`

## Main content

* Information content varies for each release and includes one or both of the following topics:
    * Improvements
    * Fixes
* Each topic includes one or more short snippet updates, unless there is nothing noteworthy to mention
* Some snippets may include animated GIFs

### Current updates

* Improvements:
    * `Added notification option for available language filters`
      * `You can now visit websites in other languages and still have the ads blocked. When you visit a website in a specific language, you will be able to choose whether you want to block ads on all websites in that language. You can turn this option on/off in <strong>Settings > General > Language filter lists</strong>. The option is already available for our users on Chrome, Firefox and Microsoft Edge and is soon coming to Opera as well.`
      * image: [/res/abp/notifications/update-page/smart-filter-lists.png](/res/abp/notifications/update-page/smart-filter-lists.png)
      * This improvement is hidden for FR and DE locales;
    * `Added quick access links to the menu footer`
      * `When you open the Adblock Plus menu from your toolbar icon, you'll see a new redesigned footer. From here, you can easily access interesting links to get more out of your product. Get support from the Help Center, get in touch with us on social media and more!`
    * `Updated filter validation messages`
      * `If you're frequently adding custom filters to ABP, you'll now see more clear error messages if there's any issues. We hope this helps!`
      
* Fixes:
    * `Updated our wording`
      * `We've updated our wording in order to use <strong>allowlist</strong> moving forward.`
    * `Add option to re-enable filters in downloadable filter lists`
      * `You will now be notified when some filters in the downloadable filter lists are turned off so that you can re-enable them at your convenience.`
    * `Fixed wrong link behind the "Rate it" button`
      * `We've updated the link behind the "Rate it" button on Microsoft Edge so that it points to the correct add-ons store.`

#### Notes for translators

* Overall tone is friendly, short, and to-the-point
* Quick snippets to inform the user of noteworthy updates

## Contribute

* Title: `Enjoying Adblock Plus?`
* Description: `Consider supporting ABP in one of two ways:`
* Description for Edge users: `Consider supporting ABP:`
* `Rate it!`
* `Please take a moment and help spread the word by rating Adblock Plus. It's quick and free!`
* Rating section is not visible for Edge users
* CTA button: `Rate it`
    * CTA button links to the relevant store, if possible to the review page.
    * [Documentation link](/spec/abp/prefs.md#documentation-link): %LINK%=chrome_review
    * [Documentation link](/spec/abp/prefs.md#documentation-link): %LINK%=firefox_review
    * [Documentation link](/spec/abp/prefs.md#documentation-link): %LINK%=opera_review
* `Contribute to the ABP project`
* `Your support allows us to continue to improve Adblock Plus.`
* CTA button: `Contribute`
    * CTA button links to: [Documentation link](/spec/abp/prefs.md#documentation-link): %LINK%=donation_update_page

## Contact us

* `We want to hear from you!`
* `Share your thoughts on the latest ABP update.`
    * Twitter icon - [Documentation link](/spec/abp/prefs.md#documentation-link): %LINK%=social_twitter in a new tab , alt text:`Twitter`
    * Facebook icon - [Documentation link](/spec/abp/prefs.md#documentation-link): %LINK%=social_facebook in a new tab , alt text: `Facebook`
    * Email - support@adblockplus.org  , alt-text:`Email`
* Copyright text `eyeo GmbH` links to [Documentation link](/spec/abp/prefs.md#documentation-link): %LINK%=eyeo
