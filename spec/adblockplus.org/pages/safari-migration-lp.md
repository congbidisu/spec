# Safari Migration landing page

## URL and locales

|Name|Value|
|----|-----|
| URL | `/safari-app-extension-migration` |
| Available in: | EN, DE, FR, NL, ES, CN, IT, RU |

---

## Meta information

|Name|Value|
|----|-----|
| title | `Adblock Plus for Safari is changing.` |
| description | `Apple’s extension changes mean we have to change Adblock Plus, but don’t worry - we’ve still got your back on Mac!` |

## Design

### Previews

* [Preview on desktop](/res/adblockplus.org/screenshots/safari-migration-lp-preview-on-desktop.png)
* [Preview on tablets](/res/adblockplus.org/screenshots/safari-migration-lp-preview-on-tablets.png)
* [Preview on mobile](/res/adblockplus.org/screenshots/safari-migration-lp-preview-on-mobile.png)

### Source file

* [Design spec](https://xd.adobe.com/spec/05ae05f4-75b3-47ea-54e0-32bf1676055e-739f/)
* [Source file](https://drive.google.com/open?id=1KgDg6MM1PgtsAY_Bf4ZwRRN0Cs7RKuK6)


## Content

### Staging/Hero area

Asset(s):

* [Product UI](/res/adblockplus.org/static/adblock-plus-for-safari-UI.png)

```
Safari is changing how extensions work

# Blocking annoying ads on Safari is still possible

Due to changes in the way Safari handles extensions, the old version of Adblock Plus for Safari will no longer work.

The good news is the all-new Adblock Plus for Safari app allows you to continue blocking annoying ads while using Safari.

[Get Adblock Plus on the App Store](https://eyeo.to/adblockplus/macos_safari_install/safari-migration-page) {: title="Get Adblock Plus on the App Store" }

![Screenshot of Adblock Plus for Safari](adblock-plus-mac-safari.png)
```

### Features section

Asset(s):

* [Rocket icon](/res/adblockplus.org/static/rocket.png)
* [Rocket icon 2x](/res/adblockplus.org/static/rocket-2x.png)
* [Octagon icon](/res/adblockplus.org/static/octagon-check.png)
* [Octagon icon 2x](/res/adblockplus.org/static/octagon-check-2x.png)

```
## Get Adblock Plus for Safari:

![Rocket](rocket-icon.svg)

### FASTER, MORE ENJOYABLE BROWSING

We’ve designed Adblock Plus for Safari to be much faster than the current Adblock Plus extension.

By blocking annoying ads such as pop-ups, video ads and banners, pages load faster.

![Checkmark in an octagon](checkmark-icon.svg)

### SUPPORT FAIR PUBLISHERS

Websites need money to stay free. Support them by allowing Acceptable Ads (on by default). Want to block all ads? No problem. [Learn how](https://adblockplus.org/en/acceptable-ads#optout){: title="Learn how to disable Acceptable Ads" }
```

**Implementation detail:** Background color for this section is `#F2F2F2`.